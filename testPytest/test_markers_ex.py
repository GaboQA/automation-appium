import pytest

@pytest.mark.functional
def test_log():
    print('Login')


@pytest.mark.regression
def test_UserReg():
    print('User Register')


@pytest.mark.functional
def test_ComposeEmail():
    print('ComposeEmail')


@pytest.mark.skip
def test_skip():
    print("Skipping test")