import pytest

def get_data():
    return [
        ('trainer1@yopmail.com','password'),
        ('trainer2@yopmail.com', 'password'),
        ('trainer3@yopmail.com', 'password'),
        ('trainer4@yopmail.com', 'password'),
        ('trainer5@yopmail.com', 'password'),
        ('trainer6@yopmail.com', 'password'),
        ('trainer7@yopmail.com', 'password'),
    ]


@pytest.mark.parametrize('username,password', get_data())
def test_dologin(username,password):
    print(username,password)