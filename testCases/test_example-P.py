import unittest
from appium import webdriver


class TestCalculator(unittest.TestCase):
    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '11.0'
        desired_caps['deviceName'] = 'HONOR X8'
        desired_caps['appPackage'] = 'com.android.calculator2'
        desired_caps['appActivity'] = 'com.android.calculator2.Calculator'
        desired_caps['noReset'] = 'true'
        desired_caps['newCommandTimeout'] = 600
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        self.driver.implicitly_wait(30)

    def tearDown(self):
        self.driver.quit()

    def test_addition(self):
        self.driver.find_element_by_id("com.android.calculator2:id/digit_2").click()
        self.driver.find_element_by_id("com.android.calculator2:id/op_add").click()
        self.driver.find_element_by_id("com.android.calculator2:id/digit_2").click()
        self.driver.find_element_by_id("com.android.calculator2:id/eq").click()
        result = self.driver.find_element_by_id("com.android.calculator2:id/result").text
        self.assertEqual(result, "4")


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCalculator)
    unittest.TextTestRunner(verbosity=2).run(suite)