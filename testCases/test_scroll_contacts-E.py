import time

from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver import TouchActions
from selenium.webdriver.common.by import By
from functions.scroll_util import ScrollUtil

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['deviceName'] = 'emulator-5554'
desired_caps['appPackage'] = 'com.android.contacts'
desired_caps['appActivity'] = 'com.android.contacts.DialtactsContactsEntryActivity'

driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
driver.implicitly_wait(10)
ScrollUtil.scrollToTextByAndroidUIAutomator("Vicky Pv", driver)
ScrollUtil.swipeUp(4,driver)
ScrollUtil.swipeDown(4,driver)


#elements = driver.find_elements(By.XPATH,'com.android.contacts:id/cliv_name_textview')
#print(len(elements))
# actions = TouchAction(driver)
# #actions.tap(elements[2])
# actions.long_press(elements[2])
# actions.perform()

time.sleep(2)
driver.quit()
