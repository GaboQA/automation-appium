import time
import pytest

from appium import webdriver
from selenium.webdriver.common.by import By

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['deviceName'] = 'HONOR X8'
desired_caps['appPackage'] = 'com.android.chrome'
desired_caps['appActivity'] = 'org.chromium.chrome.browser.ChromeTabbedActivity'

driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

driver.get('http://google.com')
time.sleep(2)
driver.find_element(By.XPATH,'//*[@class="android.widget.Button"]').click()
time.sleep(2)
driver.find_element(By.XPATH,'//android.widget.Button[@text="NO THANKS"]').click()
time.sleep(5)

#Ingresar a un Switch frame
#driver.switch_to.context('WEBVIEW_chrome')
#webview = driver.contexts[1]
#driver.switch_to.context(webview)
driver.get('http://google.com')
driver.find_element(By.ID,'com.android.chrome:id/infobar_close_button').click()
time.sleep(3)
contexts = driver.contexts
for context in contexts:
    print(context)
webview = driver.contexts[0]
driver.switch_to.context(webview)
driver.find_element(By.ID, "com.android.chrome:id/compositor_view_holder").click()
driver.find_element(By.CLASS_NAME, "android.webkit.WebView")
driver.find_element(By.XPATH, "//android.webkit.WebView[@text='Google']").click()
driver.find_element(By.CLASS_NAME, "android.view.View")
#driver.find_element(By.XPATH, "//android.view.View[@text='Imágenes']").click()
#driver.find_element(By.XPATH, "//android.view.View[@text='Todos']").click()
time.sleep(2)
#driver.find_element(By.XPATH, "//android.view.View[@text='English']").click()
driver.find_element(By.CLASS_NAME, "android.widget.EditText").click()
driver.find_element(By.CLASS_NAME, "android.widget.EditText").send_keys('Use Axios')
driver.find_element(By.CLASS_NAME, "android.widget.Button").click()
driver.find_element(By.CLASS_NAME, "android.widget.Button").click()
#driver.find_element(By.ID,'mib').send_keys('Use Axios')
#driver.find_element(By.ID,'tsbb').click()
time.sleep(10)
driver.quit()