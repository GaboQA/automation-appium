import time
import unittest
from appium import webdriver
from selenium.webdriver.common.by import By


class TestCalculator(unittest.TestCase):

    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '11.0'
        desired_caps['deviceName'] = 'HONOR X8'
        desired_caps['appPackage'] = 'com.hihonor.calculator'
        desired_caps['appActivity'] = 'com.hihonor.calculator.Calculator'
        desired_caps['noReset'] = 'true'
        desired_caps['newCommandTimeout'] = 200
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        self.driver.implicitly_wait(30)


    def test_addition(self):
        driver = self.driver
        name = 'com.hihonor.calculator'
        driver.find_element(By.ID,name+":id/digit_2").click()
        driver.find_element(By.ID,name+":id/op_add").click()
        driver.find_element(By.ID,name+":id/digit_2").click()
        driver.find_element(By.ID,name+":id/eq").click()
        result = driver.find_element(By.ID,name+":id/result").text
        print(result)
        time.sleep(2)
        self.assertEqual(result, "4")


    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCalculator)
    unittest.TextTestRunner(verbosity=2).run(suite)

