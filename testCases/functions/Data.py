import random


class Data():


    def __init__(self,driver):
        self.driver = driver


    def Data(self, const):
        driver = self.driver
        if(const == 'name'):
            NamesList = ['GonzaloQA', 'CapitanQA', 'SocioQA', 'RicardoQA', 'MariaQA', 'EduardoQA']
            contNotes = len(NamesList)
            ChooseNote = random.randint(0, contNotes)
            return NamesList[int(ChooseNote)-1]

        elif(const == 'phone'):
            number = random.randint(10000000,99999999)
            return '+506'+str(number)

        elif (const == 'notes'):
            NotesList = ['ND', 'Capitan', 'Socio', 'Colega', 'Rentador', 'Oficinista']
            contNotes = len(NotesList)
            ChooseNote = random.randint(0, contNotes)
            return NotesList[int(ChooseNote)-1]

        elif (const == 'email'):
            NotesList = ['ND', 'Capitan', 'Socio', 'Colega', 'Rentador', 'Oficinista']
            contNotes = len(NotesList)
            ChooseNote = random.randint(0, contNotes)
            numb = random.randint(1,100000)
            email = NotesList[int(ChooseNote)-1]+str(numb)+'@yopmail.com'
            return  email

