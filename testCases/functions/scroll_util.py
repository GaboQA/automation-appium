from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.common.touch_action import TouchAction


class ScrollUtil:

    @staticmethod
    def scrollToTextByAndroidUIAutomator(text,driver):
        driver.find_element(AppiumBy.ANDROID_UIAUTOMATOR,"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+text+"\").instance(0))").click()


        # driver.swipe(514,600,514,200,1000)
        # driver.swipe(514,600,514,200,1000)
        # driver.swipe(514,600,514,200,1000)
        # driver.swipe(514,600,514,200,1000)
        #
        #
        # driver.swipe(514,500,514,800,1000)
        # driver.swipe(514,500,514,800,1000)
        # driver.swipe(514,500,514,800,1000)
        # driver.swipe(514,500,514,800,1000)

        # ScrollUtil.swipeUp(4,driver)
        # ScrollUtil.swipeDown(4,driver)
        #
        # elements = driver.find_elements_by_id('com.android.contacts:id/cliv_name_textview')
        # print(len(elements))
        #
        # actions = TouchAction(driver)
        # #actions.tap(elements[2])
        # actions.long_press(elements[2])
        # actions.perform()
        #



    @staticmethod
    def swipeUp(howManySwipes,driver):
        for i in range(1,howManySwipes+1):
            driver.swipe(514, 600, 514, 200, 1000)

    @staticmethod
    def swipeDown(howManySwipes,driver):
        for i in range(1, howManySwipes + 1):
            driver.swipe(514, 500, 514, 800, 1000)

    @staticmethod
    def swipeLeft(howManySwipes,driver):
        for i in range(1, howManySwipes + 1):
            driver.swipe(900, 600, 200, 600, 1000)

    @staticmethod
    def swipeRight(howManySwipes,driver):
        for i in range(1, howManySwipes + 1):
            driver.swipe(200, 600, 900, 600, 1000)