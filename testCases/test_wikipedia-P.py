import time
from selenium.webdriver.common.by import By
from appium import webdriver
from appium.webdriver.appium_service import AppiumService, AppiumServiceError
from selenium.webdriver.support.select import Select
#from appium.webdriver.appium_service import AppiumServiceBuilder

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '11.0'
desired_caps['deviceName'] = 'HONOR X8'
desired_caps['appPackage'] = 'com.android.chrome'
desired_caps['appActivity'] = 'com.google.android.apps.chrome.Main'
desired_caps['noReset'] = 'true'
desired_caps['newCommandTimeout'] = 200

driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub',desired_caps)
driver.get("http://wikipedia.org")
time.sleep(2)
driver.find_element(By.ID,'searchInput').send_keys('Hi World')
driver.find_element(By.CSS_SELECTOR,'#button.pure-button.pure-button-primary-progressive').click()
time.sleep(2)


