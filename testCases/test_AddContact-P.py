#We add the contact to application default in our phone
#Like recomend use Apk Info to know what is the appPackage and appActivity

import time
from appium import webdriver
from selenium.webdriver.common.by import By
from functions.Data import Data


desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '11.0'
desired_caps['deviceName'] = 'HONOR X8'
desired_caps['appPackage'] = 'com.hihonor.contacts'
desired_caps['appActivity'] = 'com.android.contacts.activities.PeopleActivity'
desired_caps['noReset'] = 'true'
desired_caps['newCommandTimeout'] = 200

driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
driver.find_element(By.ID,'com.hihonor.contacts:id/hw_fab').click()

actData = Data(driver)
time.sleep(2)
Contact = actData.Data('name')
driver.find_element(By.XPATH,'//android.widget.EditText[@text="Nombre"]').send_keys(Contact)
driver.find_element(By.XPATH,'//android.widget.EditText[@text="Organización"]').send_keys(actData.Data('notes'))
driver.find_element(By.XPATH,'//android.widget.EditText[@text="Número de teléfono"]').send_keys(actData.Data('phone'))
driver.find_element(By.XPATH,'//android.widget.EditText[@text="Correo"]').send_keys(actData.Data('email'))
driver.find_element(By.XPATH,'//android.widget.EditText[@text="Notas"]').send_keys(actData.Data('notes'))
driver.find_element(By.ID,'android:id/icon2').click()
driver.find_element(By.ID,'com.hihonor.contacts:id/contact_menuitem_menu').click()
time.sleep(2)
driver.find_element(By.XPATH,'//android.widget.TextView[@text="Eliminar contacto"]').click()
driver.find_element(By.ID,'android:id/button1').click()

driver.quit()
