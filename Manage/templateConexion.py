#We can use this template to guie the others test

import time
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy

desired_caps = dict(
    deviceName='HONOR X8',
    platformName='Android',
    browserName='Chrome'
)
desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '11.0'
desired_caps['deviceName'] = 'Pixel XL API 31'
desired_caps['appPackage'] = 'com.android.chrome'
desired_caps['appActivity'] = 'com.google.android.apps.chrome.Main'
desired_caps['noReset'] = 'true'
desired_caps['newCommandTimeout'] = 200

driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)

driver.get("https://www.google.com/")
#print(driver.title)