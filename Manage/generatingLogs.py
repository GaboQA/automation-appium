import logging


'''logging.basicConfig(filename="..\\Logs\\logfile.log", format='',
                    level=logging.INFO)

log = logging.getLogger()

log.info("This is our first log")'''


def log():
    logging.basicConfig(filename="../Logs/logfile.log", format='%(asctime)s %(clientip)-15s %(user)-8s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p',
                        level=logging.INFO)

    logger = logging.getLogger()
    return logger

logger = log()
logger.info("This is a new log")
logger.error("This is a error message")