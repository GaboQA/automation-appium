import time
from appium import webdriver
from pathlib import Path
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['deviceName'] = 'emulator-5554'
#Install Apk with this Line
#desired_caps['app'] = str(Path().absolute().parent)+'\\app\\amazonDos.apk'
desired_caps['appPackage'] = 'com.amazon.mShop.android.shopping'
desired_caps['appActivity'] = 'com.amazon.mShop.home.HomeActivity'
driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
driver.implicitly_wait(11)
driver.find_element(By.ID,'com.amazon.mShop.android.shopping:id/skip_sign_in_button').click()
wait = WebDriverWait(driver,10)
wait.until(EC.element_to_be_clickable((By.ID,'com.amazon.mShop.android.shopping:id/chrome_search_hint_view')))
driver.find_element(By.ID,'com.amazon.mShop.android.shopping:id/chrome_search_hint_view').send_keys('COAT')
driver.quit()


#Documentation UISelector
#Text
#driver.find_element(By.AndroidUIAutomator('text("Button Text")'))
#Scroll
#driver.find_element(By.AndroidUIAutomator('new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains("Akash").instance(0))')).click()
