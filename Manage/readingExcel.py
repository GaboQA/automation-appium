import openpyxl

workbook = openpyxl.load_workbook("..//excel//testdata.xlsx")
sheet = workbook["Logintest"]
totalRows = sheet.max_row
totalCols = sheet.max_column


print("Total rows are : ", str(totalRows), "and total cols are", str(totalCols))
#print(sheet.cell(row=2,column=1).value)


for rows in range(1,totalRows+1):

    for column in range(1,totalCols+1):
        if(column > 0):
            print(sheet.cell(row=rows, column=column).value, end=' ')
