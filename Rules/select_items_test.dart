import 'package:base_project/common/widgets/select_items.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  final selectedItem = [];
  group("Select Items", () {
    Widget buildTestApp() {
      return MaterialApp(
        home: Directionality(
          textDirection:
              TextDirection.ltr, // Puedes ajustar esto según tu aplicación
          child: Scaffold(
            body: SelectItems(
              initialValue: null,
              onChanged: (value) {
                selectedItem.add(value);
              },
              data: const [
                {'id': 1, 'name': 'Item 1'},
                {'id': 2, 'name': 'Item 2'},
              ],
              itemKey: 'id',
              label: 'Label',
              title: 'Title',
            ),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      // Verifica que los elementos esperados estén presentes en el widget
      expect(find.text('Label'), findsOneWidget);
      expect(find.text('Title'), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets('Calls onChanged when item is selected',
        (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      // Tap the dropdown
      final dropdown = find.byKey(const Key("dropdown"));
      expect(dropdown, findsOneWidget);
      await tester.tap(dropdown);
      await tester.pumpAndSettle();

      // Tap the second item
      await tester.tap(find.text('Item 2').last);
      await tester.pump();

      // Verify that onChanged was called and selectedItem is updated
      expect(selectedItem.length, equals(1));
    });
  });
}
