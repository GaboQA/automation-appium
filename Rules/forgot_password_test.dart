import 'package:base_project/core/localization/localization.dart';
import 'package:base_project/core/services/modals_service.dart';
import 'package:base_project/modules/auth/recovery_password.dart/providers/password_recovery_form_provider.dart';
import 'package:base_project/modules/auth/recovery_password.dart/screens/recovery_password_screen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockDio extends Mock implements RecoveryPasswordFormNotifier, Dio {}

class CustomRecoveryPasswordFormNotifier extends RecoveryPasswordFormNotifier {
  CustomRecoveryPasswordFormNotifier();

  @override
  onSubmitOTP() {
    state = state.copywith(
      step: PasswordRecoverySteps.validateOTP,
      isPosting: false,
      isPosted: false,
      isvalid: false,
    );
  }

  @override
  onSubmitValidateOTP() {
    state = state.copywith(
      step: PasswordRecoverySteps.setPassword,
      isPosting: false,
      isPosted: false,
      isvalid: false,
    );
  }

  @override
  onSubmitSetPassword() {
    state = state.copywith(
      step: PasswordRecoverySteps.completed,
      isPosting: false,
      isPosted: false,
      isvalid: false,
    );
  }
}

void main() {
  group("forgot password widget tests", () {
    late CustomRecoveryPasswordFormNotifier customRecoveryPasswordFormNotifier;

    setUp(() {
      customRecoveryPasswordFormNotifier = CustomRecoveryPasswordFormNotifier();
    });
    Widget buildTestApp() {
      return MediaQuery(
        data: const MediaQueryData(),
        child: MaterialApp(
          scaffoldMessengerKey: ModalsService.messengerKey,
          supportedLocales: L10n.all,
          localizationsDelegates: const [
            L10n.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
          home: const RecoveryPasswordScreen(),
        ),
      );
    }

    testWidgets("basic test", (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      expect(find.text("Forgot your password?"), findsOneWidget);
      expect(find.text("Enter your registered e-mail"), findsOneWidget);
      expect(find.text("Email"), findsOneWidget);
      expect(find.text("Send code"), findsOneWidget);
      expect(find.text("Have an account?"), findsOneWidget);
      expect(find.text("Sign In"), findsOneWidget);

      tester.pumpAndSettle();
    });

    testWidgets("test not code", (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      final sendCode = find.text("Send code");
      expect(sendCode, findsOneWidget);

      await tester.tap(sendCode);
      await tester.pumpAndSettle();

      expect(find.text("The field is required."), findsOneWidget);
    });

    testWidgets("test steps", (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(overrides: [
        recoveryPasswordFormProvider
            .overrideWith((ref) => customRecoveryPasswordFormNotifier)
      ], child: buildTestApp()));

      final sendCode = find.byKey(const Key("send_code"));
      expect(sendCode, findsOneWidget);
      final inputEmail = find.byKey(const Key("input_email"));
      expect(inputEmail, findsOneWidget);

      await tester.enterText(inputEmail, "da.rojas12@live.com");

      await tester.tap(sendCode);
      await tester.pump();

      final valiteButton = find.byKey(const Key("validate_otp_button"));
      expect(valiteButton, findsOneWidget);
      await tester.tap(valiteButton);
      await tester.pump();

      final setPassword = find.byKey(const Key("set_password_button"));
      expect(setPassword, findsOneWidget);
      await tester.tap(setPassword);
      await tester.pump();

      final complete = find.byKey(const Key("complete_button"));
      expect(complete, findsOneWidget);
    });
  });
}
