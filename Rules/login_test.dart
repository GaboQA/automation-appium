import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:dio/dio.dart';

import 'package:base_project/core/localization/localization.dart';
import 'package:base_project/core/services/modals_service.dart';
import 'package:base_project/modules/auth/login/providers/login_form_provider.dart';
import 'package:base_project/modules/auth/login/screens/login_screen.dart';
import 'package:mocktail/mocktail.dart';

class MockDio extends Mock implements Dio {}

class MockLoginFormNotifier extends LoginFormNotifier {
  final Function(String, String) login;
  bool loginCalled = false;

  MockLoginFormNotifier({required this.login}) : super(loginCallBack: login);

  Future<void> simulateLogin(MockDio dio) async {
    when(() => dio.post("https://api-dev-basep.heippi.com/api/v1/auth/login",
            data: {"email": "yurguen0071@gmail.com", "password": "123456789"}))
        .thenAnswer((_) async => Response(
            requestOptions: RequestOptions(),
            data: {"token": "token_test"},
            statusCode: 200));

    final response = await dio.post(
        "https://api-dev-basep.heippi.com/api/v1/auth/login",
        data: {"email": "yurguen0071@gmail.com", "password": "123456789"});

    if (response.statusCode == 200) {
      loginCalled = true;
    }
  }

  @override
  void onFormSubmit() async {
    final dio = MockDio();
    simulateLogin(dio);
  }
}

void main() {
  group("Login Widget Tests", () {
    late MockLoginFormNotifier mockLoginFormNotifier;

    setUp(() {
      mockLoginFormNotifier =
          MockLoginFormNotifier(login: (email, password) {});
    });

    Widget buildTestApp(Widget child) {
      return MediaQuery(
        data: const MediaQueryData(),
        child: MaterialApp(
          scaffoldMessengerKey: ModalsService.messengerKey,
          supportedLocales: L10n.all,
          localizationsDelegates: const [
            L10n.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          home: child,
        ),
      );
    }

    testWidgets("Basic Test", (WidgetTester tester) async {
      await tester.pumpWidget(
        ProviderScope(child: buildTestApp(const LoginScreen())),
      );

      expect(find.text("Log into your account"), findsOneWidget);
      expect(find.text("Email"), findsOneWidget);
      expect(find.text("Password"), findsOneWidget);
      expect(find.text("Forgot your password?"), findsOneWidget);
      expect(find.text("Sign In"), findsOneWidget);
      expect(find.text("Don't have an account?"), findsOneWidget);
      expect(find.text("Create account"), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets("Void Inputs Test", (WidgetTester tester) async {
      await tester.pumpWidget(
        ProviderScope(child: buildTestApp(const LoginScreen())),
      );

      final button = find.byKey(const Key("login_send"));
      expect(button, findsOneWidget);

      await tester.tap(button);
      await tester.pump();

      expect(find.text("The field is required."), findsWidgets);
      expect(mockLoginFormNotifier.loginCalled, false);
      await tester.pumpAndSettle();
    });

    testWidgets("Invalid Format Test", (WidgetTester tester) async {
      await tester.pumpWidget(
        ProviderScope(child: buildTestApp(const LoginScreen())),
      );

      final email = find.byKey(const Key("email"));
      final password = find.byKey(const Key("password"));
      final button = find.byKey(const Key("login_send"));
      expect(button, findsOneWidget);
      expect(email, findsOneWidget);
      expect(password, findsOneWidget);

      await tester.enterText(email, "123");
      await tester.tap(button);
      await tester.pumpAndSettle();

      expect(find.text("The format is not valid."), findsOneWidget);
      expect(mockLoginFormNotifier.loginCalled, false);
    });

    testWidgets("Request Test", (WidgetTester tester) async {
      await tester.pumpWidget(
        ProviderScope(
          overrides: [
            loginFormProvider.overrideWith((ref) => mockLoginFormNotifier),
          ],
          child: buildTestApp(const LoginScreen()),
        ),
      );

      final email = find.byKey(const Key("email"));
      final password = find.byKey(const Key("password"));
      final button = find.byKey(const Key("login_send"));
      expect(button, findsOneWidget);
      expect(email, findsOneWidget);
      expect(password, findsOneWidget);

      await tester.enterText(email, "yurguen0071@gmail.com");
      await tester.enterText(password, "lead4dead");
      await tester.tap(button);
      await tester.pumpAndSettle();

      expect(mockLoginFormNotifier.loginCalled, true);
    });
  });
}
