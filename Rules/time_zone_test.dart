import 'package:base_project/core/localization/localization.dart';
import 'package:base_project/core/services/modals_service.dart';
import 'package:base_project/modules/time_zone/providers/timezone_provider.dart';
import 'package:base_project/modules/time_zone/screens/timezone_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

class CustomTimeZoneFormNotifier extends TimeZoneFormNotifier {
  bool isPosting = false;
  CustomTimeZoneFormNotifier();

  @override
  saveInfoTimeZoneForUser() async {
    isPosting = true;
  }
}

void main() {
  group('TimeZone page ', () {
    late CustomTimeZoneFormNotifier customTimeZoneFormNotifier;
    setUp(() {
      customTimeZoneFormNotifier = CustomTimeZoneFormNotifier();
    });
    Widget buildTestApp() {
      return ProviderScope(
        overrides: [
          timezoneFormProvider.overrideWith((ref) => customTimeZoneFormNotifier)
        ],
        child: MediaQuery(
          data: const MediaQueryData(),
          child: MaterialApp(
            scaffoldMessengerKey: ModalsService.messengerKey,
            supportedLocales: L10n.all,
            localizationsDelegates: const [
              L10n.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
            home: const TimeZoneScreen(),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());
      expect(find.text('Zona horaria'), findsOneWidget);
      await tester.pumpAndSettle();
    });
    testWidgets('renders selected timezone', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());
      expect(find.text('Seleccione una zona horaria:'), findsOneWidget);
      await tester.pumpAndSettle();
    });
    testWidgets('renders submit button', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());
      final t = L10n.translates;
      expect(find.text(t.timezone_submit_btn_label), findsOneWidget);
      await tester.pumpAndSettle();
    });

    testWidgets('Click in submit button', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());
      final editButton = find.byKey(const Key("save_timezone"));
      expect(editButton, findsOneWidget);
      await tester.tap(editButton);
      await tester.pump();
      final snack = find.byKey(const Key("snack_alert"));
      expect(snack, findsOneWidget);
      await tester.pumpAndSettle();
    });
  });
}
