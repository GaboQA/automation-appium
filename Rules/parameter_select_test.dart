// Mock de la clase ApiAdapter
import 'package:base_project/common/widgets/parameter_select.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// Mock de la clase Response para simular respuestas de API
class MockResponse extends Mock implements Response<dynamic> {}

void main() {
  late bool onChange = false;
  late ParameterSelect parameterSelect;

  setUp(() {
    parameterSelect = ParameterSelect(
      entity: 'testEntity',
      initialValue: null,
      label: 'Test Label',
      onChanged: (dynamic value) {
        onChange = true;
      },
      itemKey: 'testKey',
      inTest: true,
    );
  });

  group('ParameterSelect Widget', () {
    Widget buildTestApp() {
      return MaterialApp(
        home: Directionality(
          textDirection:
              TextDirection.ltr, // Puedes ajustar esto según tu aplicación
          child: Scaffold(body: parameterSelect),
        ),
      );
    }

    testWidgets('Should display SelectItems widget',
        (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());

      expect(find.text("Test Label"), findsOneWidget);
      await tester.pumpAndSettle();
    });

    testWidgets('Should fetch data and populate SelectItems',
        (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());

      final dropdown = find.byKey(const Key("dropdown"));
      expect(dropdown, findsOneWidget);
      await tester.tap(dropdown);
      await tester.pumpAndSettle();
      expect(find.text('Item 1'), findsOneWidget);
      expect(find.text('Item 2'), findsOneWidget);
    });

    testWidgets('On Clic Option', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());

      final dropdown = find.byKey(const Key("dropdown"));
      expect(dropdown, findsOneWidget);
      await tester.tap(dropdown);
      await tester.pumpAndSettle();

      await tester.tap(find.text('Item 2').last);
      await tester.pump();

      expect(onChange, true);
    });
  });
}
