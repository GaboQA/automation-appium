import 'package:base_project/common/widgets/file_list.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  int indexTest = -1;
  group("Select Items", () {
    Widget buildTestApp() {
      return MaterialApp(
        home: Directionality(
          textDirection:
              TextDirection.ltr, // Puedes ajustar esto según tu aplicación
          child: Scaffold(
            body: SelectedFilesList(
                onDelete: (index) {
                  indexTest = index;
                },
                selectedFiles: [
                  PlatformFile(name: "test", size: 50, path: "/test.pdf")
                ]),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      expect(find.text('test'), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets('Press button', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      final button = find.byKey(const Key("icon"));
      expect(button, findsOneWidget);

      await tester.tap(button);

      await tester.pumpAndSettle();

      expect(indexTest, equals(0));
    });
  });
}
