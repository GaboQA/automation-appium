import 'package:base_project/core/localization/localization.dart';
import 'package:base_project/core/services/modals_service.dart';
import 'package:base_project/modules/account/change-password/providers/change_password_form_provider.dart';
import 'package:base_project/modules/account/change-password/screens/change_password_screen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockDio extends Mock implements Dio {}

class MockLoginFormNotifier extends ChangePasswordFormNotifier {
  bool passwordChanged = false;

  MockLoginFormNotifier({required super.logoutCallback});

  Future<void> simulateChangePassword(MockDio dio) async {
    when(() => dio.post(
                "https://api-dev-basep.heippi.com/api/v1/auth/change-password",
                data: {
                  "current_password": "123456789",
                  "new_password": "PasswordTest01"
                }))
        .thenAnswer((_) async => Response(
            requestOptions: RequestOptions(),
            data: {"msg": "Password changed"},
            statusCode: 200));

    final response = await dio.post(
        "https://api-dev-basep.heippi.com/api/v1/auth/change-password",
        data: {
          "current_password": "123456789",
          "new_password": "PasswordTest01"
        });

    if (response.statusCode == 200) {
      passwordChanged = true;
    }
  }

  @override
  void onFormSubmit() async {
    final dio = MockDio();
    simulateChangePassword(dio);
  }
}

void main() {
  group('ChangePassword widget', () {
    late MockLoginFormNotifier mockLoginFormNotifier;
    setUp(() {
      mockLoginFormNotifier = MockLoginFormNotifier(logoutCallback: () {});
    });
    Widget buildTestApp(Widget child) {
      return MaterialApp(
        scaffoldMessengerKey: ModalsService.messengerKey,
        supportedLocales: L10n.all,
        localizationsDelegates: const [
          L10n.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        home: child,
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(
          ProviderScope(child: buildTestApp(const ChangePassword())));

      // Verifica que los elementos esperados estén presentes en el widget
      expect(find.text('Current password'), findsOneWidget);
      expect(find.text('New password'), findsOneWidget);
      expect(find.text('Confirm new password'), findsOneWidget);
      expect(find.byKey(const Key("saveButton")), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets("test validations void", (WidgetTester tester) async {
      await tester.pumpWidget(
          ProviderScope(child: buildTestApp(const ChangePassword())));

      final buttonSave = find.byKey(const Key("saveButton"));
      expect(buttonSave, findsOneWidget);
      await tester.tap(buttonSave);
      await tester.pumpAndSettle();

      expect(find.text("The field is required."), findsWidgets);
    });

    testWidgets("test validations length password",
        (WidgetTester tester) async {
      await tester.pumpWidget(
          ProviderScope(child: buildTestApp(const ChangePassword())));

      final buttonSave = find.byKey(const Key("saveButton"));
      final currentPassword = find.byKey(const Key("currentPasswordTextField"));
      final newPassword = find.byKey(const Key("newPasswordTextField"));
      final confirmPassword = find.byKey(const Key("confirmPasswordTextField"));
      expect(buttonSave, findsOneWidget);
      expect(currentPassword, findsOneWidget);
      expect(newPassword, findsOneWidget);
      expect(confirmPassword, findsOneWidget);

      await tester.enterText(currentPassword, "1");
      await tester.enterText(newPassword, "2");
      await tester.enterText(confirmPassword, "3");
      await tester.tap(buttonSave);
      await tester.pumpAndSettle();

      expect(find.text("Min 6 characters required."), findsWidgets);
    });

    testWidgets('taps on save button', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(overrides: [
        changePasswordFormProvider.overrideWith((ref) => mockLoginFormNotifier)
      ], child: buildTestApp(const ChangePassword())));

      final currentPassword = find.byKey(const Key('currentPasswordTextField'));
      await tester.enterText(currentPassword, 'contraseña_actual');
      await tester.enterText(
        find.byKey(const Key('newPasswordTextField')),
        'nueva_contraseña',
      );
      await tester.enterText(
        find.byKey(const Key('confirmPasswordTextField')),
        'nueva_contraseña',
      );

      final buttonSave = find.byKey(const Key("saveButton"));
      expect(buttonSave, findsOneWidget);

      await tester.tap(buttonSave);
      await tester.pump();

      expect(mockLoginFormNotifier.passwordChanged, true);
    });
  });
}
