import 'package:base_project/common/models/models.dart';
import 'package:base_project/core/localization/localization.dart';
import 'package:base_project/core/services/modals_service.dart';
import 'package:base_project/modules/account/profile/providers/profile_form_provider.dart';
import 'package:base_project/modules/account/profile/screens/profile_screen.dart';
import 'package:base_project/modules/auth/common/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

class CustomProfileFormNotifier extends ProfileFormNotifier {
  bool changeData = false;
  CustomProfileFormNotifier(
      {required super.currentUser, required super.refreshInfoCallback});

  @override
  onFormSubmit() {
    changeData = true;
  }
}

void main() {
  group('Profile ', () {
    late CustomProfileFormNotifier customProfileFormNotifier;

    setUp(() {
      customProfileFormNotifier = CustomProfileFormNotifier(
          currentUser: UserModel(
              createdAt: DateTime.now(),
              email: "da.rojas12@live.com",
              id: 1,
              isActive: true,
              isHyperadmin: true,
              person: Person(
                  address: "Av. Central",
                  birthdate: DateTime(1990, 6, 23),
                  firstName: "David",
                  lastName: "Rojas",
                  gender: Gender(pronoun: "Masculino", type: "Hombre"),
                  id: 1,
                  identityDocument: Identity(
                      identityType: IdentityType(code: "TI"),
                      number: "123456789"),
                  phone: Phone(
                      number: "1234567890", countryCode: "KZ", dialCode: "7"))),
          refreshInfoCallback: () {});
    });
    Widget buildTestApp() {
      return ProviderScope(
        overrides: [
          profileFormProvider.overrideWith((ref) => customProfileFormNotifier)
        ],
        child: MediaQuery(
          data: const MediaQueryData(),
          child: MaterialApp(
            scaffoldMessengerKey: ModalsService.messengerKey,
            supportedLocales: L10n.all,
            localizationsDelegates: const [
              L10n.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
            home: const UserProfile(),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());

      expect(find.text('Profile User'), findsOneWidget);
      expect(find.text('Profile User'), findsOneWidget);
      expect(find.text('Name'), findsOneWidget);
      expect(find.text('Lastname'), findsOneWidget);
      expect(find.text('Indentity document'), findsOneWidget);
      expect(find.text('Number'), findsOneWidget);
      expect(find.text('Phone number'), findsOneWidget);
      expect(find.text('Gender'), findsOneWidget);
      expect(find.text('Birthday'), findsOneWidget);
      expect(find.text('Address'), findsOneWidget);
      expect(find.text('Save changes'), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets("test user data", (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());

      final profileImage = find.byType(CircleAvatar);

      expect(profileImage, findsOneWidget);
      expect(find.text('David'), findsOneWidget);
      expect(find.text('Rojas'), findsOneWidget);
      expect(find.text('123456789'), findsOneWidget);
      expect(find.text('1234567890'), findsOneWidget);
      expect(find.text('1990-06-23'), findsOneWidget);
      expect(find.text('Av. Central'), findsOneWidget);

      final editButton = find.byKey(const Key("save_changes"));
      expect(editButton, findsOneWidget);

      final listFinder = find.byType(Scrollable).last;
      expect(listFinder, findsOneWidget);
      await tester.scrollUntilVisible(editButton, 200, scrollable: listFinder);

      await tester.tap(editButton);
      await tester.pump();

      expect(customProfileFormNotifier.changeData, true);

      await tester.pumpAndSettle();
    });
  });
}
