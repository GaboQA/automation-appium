import 'package:base_project/common/widgets/download_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("Select Items", () {
    Widget buildTestApp() {
      return const MaterialApp(
        home: Directionality(
          textDirection:
              TextDirection.ltr, // Puedes ajustar esto según tu aplicación
          child: Scaffold(
            body: FileDownloaderButton(
              buttonText: "Test",
              fileUrl: "https://www.orimi.com/pdf-test.pdf",
              disabled: false,
            ),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      // Verifica que los elementos esperados estén presentes en el widget
      expect(find.text('Test'), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets('Press button', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      final button = find.byKey(const Key("button_download"));
      expect(button, findsOneWidget);

      await tester.tap(button);

      await tester.pump();

      final progress = find.byKey(const Key("progress"));
      expect(progress, findsOneWidget);
    });
  });
}
