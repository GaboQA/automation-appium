import 'package:base_project/common/widgets/filter_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  String onChangeFilter = '';
  String onChangeInput = '';
  bool onSearch = false;
  group("Filter List", () {
    Widget buildTestApp() {
      return MaterialApp(
        home: Directionality(
          textDirection:
              TextDirection.ltr, // Puedes ajustar esto según tu aplicación
          child: Scaffold(
            body: FilterWidget(
              data: const [
                {"id": "1", "name": "Test 1"},
                {"id": "2", "name": "Test 2"}
              ],
              onChangeFilter: (value) {
                onChangeFilter = value;
              },
              onChangeInput: (value) {
                onChangeInput = value;
              },
              onSearch: () {
                onSearch = true;
              },
            ),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      expect(find.text('Filtros'), findsOneWidget);
      expect(find.byKey(const Key("selected")), findsOneWidget);
      expect(find.byKey(const Key("search_input")), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets('on selected', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      final selected = find.byKey(const Key("dropdown"));
      expect(selected, findsOneWidget);

      await tester.tap(selected);
      await tester.pumpAndSettle();

      final dropdownOption = find.byKey(const Key("1"));
      expect(dropdownOption, findsOneWidget);

      await tester.tap(dropdownOption);
      await tester.pumpAndSettle();

      expect(onChangeFilter, "1");
    });

    testWidgets('on changeInput', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      final searchInput = find.byKey(const Key("search_input"));
      expect(searchInput, findsOneWidget);

      await tester.enterText(searchInput, "Test change");
      await tester.pumpAndSettle();

      expect(onChangeInput, "Test change");
    });

    testWidgets('on search', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      final searchInput = find.byKey(const Key("search_input"));
      expect(searchInput, findsOneWidget);

      await tester.enterText(searchInput, "Test change");
      await tester.showKeyboard(searchInput);
      await tester.pumpAndSettle();
      await tester.testTextInput.receiveAction(TextInputAction.done);
      await tester.pump();

      expect(onSearch, true);
    });
  });
}
