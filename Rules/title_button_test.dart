import 'package:base_project/common/widgets/title_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  var onPress = false;
  group("Select Items", () {
    Widget buildTestApp() {
      return MaterialApp(
        home: Directionality(
          textDirection:
              TextDirection.ltr, // Puedes ajustar esto según tu aplicación
          child: Scaffold(
            body: TitleButton(
              onPress: () {
                onPress = true;
              },
              title: 'Test',
            ),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      expect(find.text('Test'), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets('onPress button', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      final button = find.byKey(const Key("icon_button"));
      expect(button, findsOneWidget);

      await tester.tap(button);
      await tester.pumpAndSettle();

      expect(onPress, true);
    });
  });
}
