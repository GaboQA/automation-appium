import 'package:base_project/common/widgets/card_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  bool onTapCard = false;
  bool onTapDelete = false;
  group("Card Items", () {
    Widget buildTestApp() {
      return MaterialApp(
        home: Directionality(
          textDirection:
              TextDirection.ltr, // Puedes ajustar esto según tu aplicación
          child: Scaffold(
            body: CardList(
              key: const Key("widget_card"),
              description: "Test description",
              onClick: () {
                onTapCard = true;
              },
              onDelete: () {
                onTapDelete = true;
              },
              subtitle: "Subtitle test",
              title: 'Title',
            ),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      expect(find.text('Test description'), findsOneWidget);
      expect(find.text('Subtitle test'), findsOneWidget);
      expect(find.text('Title'), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets("clic on card", (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      final card = find.byKey(const Key("widget_card"));
      expect(card, findsOneWidget);

      await tester.tap(card);
      expect(onTapCard, true);
    });

    testWidgets("clic on delete card", (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      final buttoDelete = find.byKey(const Key("delete_Title"));
      expect(buttoDelete, findsOneWidget);

      await tester.tap(buttoDelete);
      expect(onTapDelete, true);
    });
  });
}
