import 'package:base_project/modules/masters/providers/masters_provider.dart';
import 'package:base_project/modules/masters/screens/master_detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

class CustomMasterFormNotifier extends MasterFormNotifier {
  bool changeData = false;

  @override
  putMaster() {
    changeData = true;
  }
}

void main() {
  group("Select Items", () {
    late CustomMasterFormNotifier customMasterFormNotifier;
    setUp(() {
      customMasterFormNotifier = CustomMasterFormNotifier();
      customMasterFormNotifier.onItemChange({
        "id": "1",
        "name": "Test",
        "code": "codetest",
        "created_at": "10/20/2023",
      });
    });
    Widget buildTestApp() {
      return ProviderScope(
        overrides: [
          masterFormProvider.overrideWith((ref) => customMasterFormNotifier)
        ],
        child: const MaterialApp(
          home: Directionality(
            textDirection:
                TextDirection.ltr, // Puedes ajustar esto según tu aplicación
            child: Scaffold(
              body: MasterDetailScreen(),
            ),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());
      expect(find.text('Test'), findsOneWidget);
      expect(find.text('codetest'), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets('validate values', (WidgetTester tester) async {
      customMasterFormNotifier.onItemChange({
        "id": "1",
        "name": "",
        "code": "",
        "created_at": "10/20/2023",
      });
      await tester.pumpWidget(buildTestApp());

      final nameInput = find.byKey(const Key("name"));
      final codeInput = find.byKey(const Key("code"));
      final button = find.byKey(const Key("button_save"));
      expect(nameInput, findsOneWidget);
      expect(button, findsOneWidget);

      expect(codeInput, findsOneWidget);

      await tester.tap(button);
      await tester.pump();

      expect(find.text('El campo es requerido'), findsWidgets);
    });
    testWidgets('change values', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());

      final nameInput = find.byKey(const Key("name"));
      final codeInput = find.byKey(const Key("code"));

      expect(nameInput, findsOneWidget);
      expect(codeInput, findsOneWidget);

      await tester.enterText(nameInput, "Name change");
      await tester.enterText(codeInput, "Code change");

      expect(find.text('Name change'), findsOneWidget);
      expect(find.text('Code change'), findsOneWidget);
      await tester.pumpAndSettle();
    });

    testWidgets('onclick button', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestApp());

      final button = find.byKey(const Key("button_save"));
      expect(button, findsOneWidget);

      await tester.tap(button);
      await tester.pump();

      expect(customMasterFormNotifier.changeData, true);
    });
  });
}
