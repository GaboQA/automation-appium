import 'package:base_project/common/widgets/infinite_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  bool onScrollEnd = false;
  bool onTapCard = false;
  bool onTapDelete = false;
  final List<dynamic> dataTest = [
    {"id": "1", "name": "test1", "descripcion": "test desciprion 1"},
    {"id": "2", "name": "test2", "descripcion": "test desciprion 2"},
    {"id": "3", "name": "test3", "descripcion": "test desciprion 3"},
    {"id": "4", "name": "test4", "descripcion": "test desciprion 4"},
    {"id": "5", "name": "test5", "descripcion": "test desciprion 5"},
    {"id": "6", "name": "test6", "descripcion": "test desciprion 6"},
    {"id": "7", "name": "test7", "descripcion": "test desciprion 7"},
    {"id": "8", "name": "test8", "descripcion": "test desciprion 8"},
    {"id": "9", "name": "test9", "descripcion": "test desciprion 9"}
  ];
  group("Infinite list", () {
    Widget buildTestApp() {
      return MaterialApp(
        home: Directionality(
          textDirection:
              TextDirection.ltr, // Puedes ajustar esto según tu aplicación
          child: Scaffold(
            body: InfiniteList(
              data: dataTest,
              loading: true,
              onScrollEnd: (scrollInfo) {
                if (scrollInfo.metrics.pixels ==
                    scrollInfo.metrics.maxScrollExtent) {
                  onScrollEnd = true;

                  return true;
                }
                onScrollEnd = false;

                return false;
              },
              onTapCard: (item) {
                onTapCard = true;
              },
              onDeleteItem: (item) {
                onTapDelete = true;
              },
              properties: const ["id", "name", "descripcion"],
            ),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      // Verifica que los elementos esperados estén presentes en el widget
      expect(find.text('test1'), findsOneWidget);
      expect(find.text('test2'), findsOneWidget);
      expect(find.text('test3'), findsOneWidget);
    });

    testWidgets("clic on card", (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      final card = find.byKey(const Key("1"));
      expect(card, findsOneWidget);

      await tester.tap(card);
      expect(onTapCard, true);
    });

    testWidgets("clic on delete card", (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      final buttoDelete = find.byKey(const Key("delete_7"));
      expect(buttoDelete, findsOneWidget);

      await tester.tap(buttoDelete);
      expect(onTapDelete, true);
    });

    testWidgets("onAction end", (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));
      final lastCard = find.byKey(const Key("9"));
      final listFinder = find.byType(Scrollable).last;
      expect(listFinder, findsOneWidget);
      await tester.scrollUntilVisible(lastCard, 200, scrollable: listFinder);
      await tester.pump();
      expect(lastCard, findsOneWidget);
      expect(onScrollEnd, true);
    });
  });
}
