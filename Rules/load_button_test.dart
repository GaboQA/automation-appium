import 'package:base_project/common/widgets/load_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("Select Items", () {
    Widget buildTestApp() {
      return MaterialApp(
        home: Directionality(
          textDirection:
              TextDirection.ltr, // Puedes ajustar esto según tu aplicación
          child: Scaffold(
            body: LoadButton(
              title: "Test",
              allowedExtensions: const ["pdf"],
              multiple: false,
              onSendFiles: (files) {},
            ),
          ),
        ),
      );
    }

    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      // Verifica que los elementos esperados estén presentes en el widget
      expect(find.text('Test'), findsOneWidget);

      await tester.pumpAndSettle();
    });

    testWidgets('Press button', (WidgetTester tester) async {
      await tester.pumpWidget(ProviderScope(child: buildTestApp()));

      final button = find.byKey(const Key("button_load"));
      expect(button, findsOneWidget);

      await tester.tap(button);

      await tester.pumpAndSettle();
    });
  });
}
