import 'package:base_project/core/localization/localization.dart';
import 'package:base_project/core/services/modals_service.dart';
import 'package:base_project/modules/auth/create_account/screens/create_account_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("create account test", () {
    Widget buildTestApp(Widget child) {
      return MaterialApp(
        scaffoldMessengerKey: ModalsService.messengerKey,
        supportedLocales: L10n.all,
        localizationsDelegates: const [
          L10n.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        home: child,
      );
    }

    testWidgets("basic test", (WidgetTester tester) async {
      await tester.pumpWidget(
          ProviderScope(child: buildTestApp(const CreateAccountScreen())));

      expect(find.text("Create new account"), findsOneWidget);
      expect(find.text('Name'), findsOneWidget);
      expect(find.text('Lastname'), findsOneWidget);
      expect(find.text('Indentity document'), findsOneWidget);
      expect(find.text('Number'), findsOneWidget);
      expect(find.text('Phone number'), findsOneWidget);
      expect(find.text('Gender'), findsOneWidget);
      expect(find.text('Birthday'), findsOneWidget);
      expect(find.text('Address'), findsOneWidget);
      expect(find.text('Create account'), findsOneWidget);
    });

    testWidgets("Invalid required ", (WidgetTester tester) async {
      await tester.pumpWidget(
          ProviderScope(child: buildTestApp(const CreateAccountScreen())));
      await tester.pumpAndSettle();

      final button = find.byKey(const Key("button_create"));
      expect(button, findsOneWidget);
      final listFinder = find.byType(Scrollable).last;
      expect(listFinder, findsOneWidget);
      await tester.scrollUntilVisible(button, 200, scrollable: listFinder);
      await tester.tap(button);
      await tester.pumpAndSettle();

      expect(find.text("The field is required."), findsWidgets);
      expect(find.text("The format is not valid."), findsOneWidget);
      expect(find.text("The gender is required"), findsOneWidget);
      expect(find.text("The identity document is required"), findsOneWidget);
    });

    testWidgets("Validate password", (WidgetTester tester) async {
      await tester.pumpWidget(
          ProviderScope(child: buildTestApp(const CreateAccountScreen())));
      await tester.pumpAndSettle();

      final button = find.byKey(const Key("button_create"));
      final password = find.byKey(const Key("password"));
      final email = find.byKey(const Key("email"));

      final listFinder = find.byType(Scrollable).last;
      expect(listFinder, findsOneWidget);
      await tester.scrollUntilVisible(button, 200, scrollable: listFinder);

      expect(button, findsOneWidget);
      expect(password, findsOneWidget);
      expect(email, findsOneWidget);

      await tester.enterText(email, "test");
      await tester.enterText(password, "12");

      await tester.tap(button);
      await tester.pumpAndSettle();

      expect(find.text("The format is not valid."), findsWidgets);
      expect(find.text("Min 6 characters required."), findsOneWidget);

      await tester.enterText(password, "12345678");
      await tester.pumpAndSettle();
      expect(
          find.text('Invalid format, required minimum:\n'
              '- 1 uppercase letter\n'
              '- 1 lowercase letter\n'
              '- 1 special character\n'
              '- 1 number'),
          findsWidgets);
    });
  });
}
